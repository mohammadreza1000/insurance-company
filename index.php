<?php

// DEBUGING
ini_set( 'display_errors', '1' );

define( 'PHP_VALID', true );
define( 'DOC_ROOT', dirname( __FILE__ ) );

require_once DOC_ROOT . '/app/class-app.php';

App::i()->setup(
	new mysqli( 'localhost', 'root', '', 'mvc' ),
	DOC_ROOT,
	( ! empty( $_SERVER['HTTPS'] ) ? 'https' : 'http' ) . '://' . $_SERVER['HTTP_HOST'] .
		str_replace( $_SERVER['DOCUMENT_ROOT'], '', str_replace( '\\', '/', strtolower( DOC_ROOT ) ) )
);
