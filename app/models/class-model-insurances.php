<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class Model_Insurances {
	public function get_all_insurances() {
		return App::i()->get_mysqli_link()->query(
			'SELECT * FROM `insurances`
			ORDER BY `id`'
		);
	}

	public function insert_new_insurance( $datetime, $datetime2, $added_by, $for_whome ) {
		return App::i()->get_mysqli_link()->query(
			"INSERT INTO `insurances` (`datetime`, `datetime2`, `added_by`, `for_whome`)
			VALUES ('$datetime', '$datetime2', $added_by, $for_whome)"
		);
	}

	public function delete_insurance( $insuranceid ) {
		return App::i()->get_mysqli_link()->query(
			"DELETE FROM `insurances` 
			WHERE `id` = $insuranceid"
		);
	}
}
