<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class Model_Clients {
	public function get_all_clients() {
		return App::i()->get_mysqli_link()->query(
			'SELECT * FROM `clients`
			ORDER BY `id`'
		);
	}

	public function insert_new_client( $firstname, $lastname, $national_code, $father_name, $phone_no, $registered_by
	) {
		return App::i()->get_mysqli_link()->query(
			"INSERT INTO `clients` (`firstname`, `lastname`, `national_code`, `father_name`, `phone_no`,
				`registered_by`)
			VALUES ('$firstname', '$lastname', '$national_code', '$father_name', '$phone_no', $registered_by)"
		);
	}

	public function delete_client( $clientid ) {
		return App::i()->get_mysqli_link()->query(
			"DELETE FROM `clients` 
			WHERE `id` = $clientid"
		);
	}

	public function get_client_basic_info_by_id( $clientid ) {
		return App::i()->get_mysqli_link()->query(
			"SELECT `firstname`, `lastname`, `national_code` FROM `clients`
			WHERE `id` = $clientid"
		);
	}

	public function get_client_id_by_national_code( $national_code ) {
		return App::i()->get_mysqli_link()->query(
			"SELECT `id` FROM `clients`
			WHERE `national_code` = $national_code"
		);
	}
}
