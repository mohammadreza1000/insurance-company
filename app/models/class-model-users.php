<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class Model_Users {
	public function get_user_auth( $username ) {
		return App::i()->get_mysqli_link()->query(
			"SELECT `id`, `username`, `password` FROM `users`
			WHERE `username` =  '$username'"
		);
	}

	public function update_last_login( $userid ) {
		return App::i()->get_mysqli_link()->query(
			"UPDATE `users`
			SET `last_login` = '" . date( 'Y-m-d H:i:s' ) . "'
			WHERE `id` = $userid"
		);
	}

	public function get_all_users() {
		return App::i()->get_mysqli_link()->query(
			'SELECT * FROM `users`
			ORDER BY `id`'
		);
	}

	public function insert_new_user( $username, $password, $email, $firstname, $lastname, $national_code, $father_name,
		$phone_no
	) {
		return App::i()->get_mysqli_link()->query(
			"INSERT INTO `users` (`username`, `password`, `email`, `firstname`, `lastname`, `national_code`,
			 `father_name`, `phone_no`)
			VALUES ('$username', '$password', '$email', '$firstname', '$lastname', '$national_code', '$father_name', 
				'$phone_no')"
		);
	}

	public function delete_user( $userid ) {
		return App::i()->get_mysqli_link()->query(
			"DELETE FROM `users` 
			WHERE `id` = $userid"
		);
	}

	public function get_username_by_id( $userid ) {
		return App::i()->get_mysqli_link()->query(
			"SELECT `username` FROM `users` 
			WHERE `id` = $userid"
		);
	}
}
