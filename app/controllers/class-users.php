<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class Users {
	private $users_model;

	public function __construct() {
		$this->users_model = App::i()->model( 'users' );
	}

	public function index() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$result      = $this->users_model->get_all_users();
		$users_array = array();

		while ( $row = $result->fetch_assoc() ) {
			$last_login_c  = new DateTime( $row['last_login'] );
			$last_login    = '0000-00-00 00:00:00' !== $row['last_login'] ?
				App::i()->get_datetime_formatter()->format( $last_login_c ) : '-';
			$users_array[] = array(
				'id'            => $row['id'],
				'username'      => $row['username'],
				'email'         => $row['email'],
				'firstname'     => $row['firstname'],
				'lastname'      => $row['lastname'],
				'national_code' => $row['national_code'],
				'father_name'   => $row['father_name'],
				'phone_no'      => $row['phone_no'],
				'last_login'    => $last_login,
			);
		}
		App::i()->set_view_data( array(
			'users' => $users_array,
		) );

		App::i()->view();
		App::i()->view( 'home/footer' );
	}

	public function add() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$username      = App::i()->filter_input_post( 'username' );
		$password      = App::i()->filter_input_post( 'password' );
		$email         = App::i()->filter_input_post( 'email' );
		$firstname     = App::i()->filter_input_post( 'firstname' );
		$lastname      = App::i()->filter_input_post( 'lastname' );
		$national_code = App::i()->filter_input_post( 'national_code' );
		$father_name   = App::i()->filter_input_post( 'father_name' );
		$phone_no      = App::i()->filter_input_post( 'phone_no' );
		$submit        = App::i()->filter_input_post( 'submit' );

		$view = null;

		$view_data_array = array(
			'username_val'      => '',
			'password_val'      => '',
			'email_val'         => '',
			'firstname_val'     => '',
			'lastname_val'      => '',
			'national_code_val' => '',
			'father_name_val'   => '',
			'phone_no_val'      => '',
			'error_messages'    => array(),
		);

		if ( isset( $submit ) && isset( $username ) && isset( $password ) && isset( $email ) &&
			isset( $firstname ) && isset( $lastname ) && isset( $national_code ) && isset( $father_name ) &&
			isset( $phone_no )
		) {
			$error_messages = $this->validate_inputs(
				$username, $password, $email, $firstname, $lastname, $national_code, $father_name, $phone_no
			);

			if ( empty( $error_messages ) ) {
				$result = $this->users_model->insert_new_user(
					$username, $password, $email, $firstname, $lastname, $national_code, $father_name, $phone_no
				);

				if ( false !== $result ) {
					$view_data_array = array(
						'message' => 'عملیات با موفقیت انجام شد.',
					);
				} else {
					$view_data_array = array(
						'message' => 'خطایی رخ داده است ! شاید شما فیلدهای کلیدی را تکراری وارد کرده‌اید.',
					);
				}

				$view = 'home/message';
			} else {
				$view_data_array = array(
					'username_val'      => $username,
					'password_val'      => $password,
					'email_val'         => $email,
					'firstname_val'     => $firstname,
					'lastname_val'      => $lastname,
					'national_code_val' => $national_code,
					'father_name_val'   => $father_name,
					'phone_no_val'      => $phone_no,
					'error_messages'    => $error_messages,
				);
			}
		}

		App::i()->set_view_data( $view_data_array );
		App::i()->view( $view );
		App::i()->view( 'home/footer' );
	}

	public function delete() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$userid = App::i()->filter_input_get( 'id' );

		$result = $this->users_model->delete_user( $userid );

		if ( false !== $result ) {
			$view_data_array = array(
				'message' => 'عملیات با موفقیت انجام شد.',
			);
		} else {
			$view_data_array = array(
				'message' => 'خطایی رخ داده است !',
			);
		}

		App::i()->set_view_data( $view_data_array );
		App::i()->view( 'home/message' );
		App::i()->view( 'home/footer' );
	}

	protected function validate_inputs( $username, $password, $email, $firstname, $lastname, $national_code, $father_name,
		$phone_no
	) {
		$test_results   = array();
		$error_messages = array();

		$test_results[] = App::i()->validate_input( $username, 3, 22, 'نام کاربری', '3' );
		$test_results[] = App::i()->validate_input( $password, 3, 60, 'کلمه عبور' );
		$test_results[] = App::i()->validate_input( $email, 3, 60, 'پست الکترونیک', '1' );
		$test_results[] = App::i()->validate_input( $firstname, 3, 12, 'نام', '5' );
		$test_results[] = App::i()->validate_input( $lastname, 3, 24, 'نام خانوادگی', '5' );
		$test_results[] = App::i()->validate_input( $national_code, 10, 10, 'کد ملی', '4' );
		$test_results[] = App::i()->validate_input( $father_name, 3, 12, 'نام پدر', '5' );
		$test_results[] = App::i()->validate_input( $phone_no, 11, 11, 'شماره تماس', '4' );

		foreach ( $test_results as $key => $value ) {
			if ( true !== $value ) {
				$error_messages[]['error_message'] = $value;
			}
		}

		if ( ! empty( $error_messages ) ) {
			return $error_messages;
		}

		return array();
	}
}
