<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class Home {
	private $users_model;

	public function __construct() {
		$this->users_model = App::i()->model( 'users' );
	}

	public function index() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		$view           = null;
		$error_messages = array();

		if ( ! isset( $_SESSION['userid'] ) ) {
			$username = App::i()->filter_input_post( 'username' );
			$password = App::i()->filter_input_post( 'password' );
			$submit   = App::i()->filter_input_post( 'submit' );

			if ( isset( $submit ) && isset( $username ) && isset( $password ) ) {
				$userid = $this->check_auth( $username, $password );

				if ( false === $userid ) {
					$error_messages[] = array(
						'error_message' => 'نام کاربری یا کلمه عبور وارد شده معتبر نمی‌باشد.',
					);
				} else {
					$_SESSION['userid'] = $userid;
					$view               = 'home/loggedin';

					$this->users_model->update_last_login( $userid );
				}
			}
		} else {
			$view = 'home/loggedin';
		}

		App::i()->set_view_data( array(
			'error_messages' => $error_messages,
		) );

		App::i()->view( $view );
		App::i()->view( 'home/footer' );
	}

	public function logout() {
		unset( $_SESSION['userid'] );
		session_destroy();
		header( 'Location: ' . App::i()->get_root_url() );
	}

	protected function check_auth( $username, $password ) {
		if ( mb_strlen( $username ) >= 3 && mb_strlen( $username ) <= 22 &&
			mb_strlen( $password ) >= 3 && mb_strlen( $password ) <= 60
		) {
			$result = $this->users_model->get_user_auth( $username );

			while ( $row = $result->fetch_assoc() ) {
				if ( password_verify( $password, $row['password'] ) ) {
					return $row['id'];
				}
			}
		}

		return false;
	}
}
