<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class Clients {
	private $clients_model;
	private $users_model;

	public function __construct() {
		$this->clients_model = App::i()->model( 'clients' );
		$this->users_model   = App::i()->model( 'users' );
	}

	public function index() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$result        = $this->clients_model->get_all_clients();
		$clients_array = array();

		while ( $row = $result->fetch_assoc() ) {
			$registered_datetime_c = new DateTime( $row['registered_datetime'] );
			$registered_datetime   = '0000-00-00 00:00:00' !== $row['registered_datetime'] ?
				App::i()->get_datetime_formatter()->format( $registered_datetime_c ) : '-';
			$registered_by_result  = $this->users_model->get_username_by_id( $row['registered_by'] );

			while ( $subrow = $registered_by_result->fetch_assoc() ) {
				$registered_by = $subrow['username'];
			}

			$clients_array[] = array(
				'id'                  => $row['id'],
				'firstname'           => $row['firstname'],
				'lastname'            => $row['lastname'],
				'national_code'       => $row['national_code'],
				'father_name'         => $row['father_name'],
				'phone_no'            => $row['phone_no'],
				'registered_datetime' => $registered_datetime,
				'registered_by'       => $registered_by,
			);
		}
		App::i()->set_view_data( array(
			'clients' => $clients_array,
		) );

		App::i()->view();
		App::i()->view( 'home/footer' );
	}

	public function add() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$firstname     = App::i()->filter_input_post( 'firstname' );
		$lastname      = App::i()->filter_input_post( 'lastname' );
		$national_code = App::i()->filter_input_post( 'national_code' );
		$father_name   = App::i()->filter_input_post( 'father_name' );
		$phone_no      = App::i()->filter_input_post( 'phone_no' );
		$submit        = App::i()->filter_input_post( 'submit' );

		$view = null;

		$view_data_array = array(
			'firstname_val'     => '',
			'lastname_val'      => '',
			'national_code_val' => '',
			'father_name_val'   => '',
			'phone_no_val'      => '',
			'error_messages'    => array(),
		);

		if ( isset( $submit ) && isset( $firstname ) && isset( $lastname ) && isset( $national_code ) &&
			isset( $father_name ) && isset( $phone_no )
		) {
			$error_messages = $this->validate_inputs(
				$firstname, $lastname, $national_code, $father_name, $phone_no
			);

			$registered_by = intval( $_SESSION['userid'] );

			if ( empty( $error_messages ) ) {
				$result = $this->clients_model->insert_new_client(
					$firstname, $lastname, $national_code, $father_name, $phone_no, $registered_by
				);

				if ( false !== $result ) {
					$view_data_array = array(
						'message' => 'عملیات با موفقیت انجام شد.',
					);
				} else {
					$view_data_array = array(
						'message' => 'خطایی رخ داده است ! شاید شما فیلدهای کلیدی را تکراری وارد کرده‌اید.',
					);
				}

				$view = 'home/message';
			} else {
				$view_data_array = array(
					'firstname_val'     => $firstname,
					'lastname_val'      => $lastname,
					'national_code_val' => $national_code,
					'father_name_val'   => $father_name,
					'phone_no_val'      => $phone_no,
					'error_messages'    => $error_messages,
				);
			}
		}

		App::i()->set_view_data( $view_data_array );
		App::i()->view( $view );
		App::i()->view( 'home/footer' );
	}

	public function delete() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$clientid = App::i()->filter_input_get( 'id' );

		$result = $this->clients_model->delete_client( $clientid );

		if ( false !== $result ) {
			$view_data_array = array(
				'message' => 'عملیات با موفقیت انجام شد.',
			);
		} else {
			$view_data_array = array(
				'message' => 'خطایی رخ داده است !',
			);
		}

		App::i()->set_view_data( $view_data_array );
		App::i()->view( 'home/message' );
		App::i()->view( 'home/footer' );
	}

	protected function validate_inputs( $firstname, $lastname, $national_code, $father_name, $phone_no ) {
		$test_results   = array();
		$error_messages = array();

		$test_results[] = App::i()->validate_input( $firstname, 3, 12, 'نام', '5' );
		$test_results[] = App::i()->validate_input( $lastname, 3, 24, 'نام خانوادگی', '5' );
		$test_results[] = App::i()->validate_input( $national_code, 10, 10, 'کد ملی', '4' );
		$test_results[] = App::i()->validate_input( $father_name, 3, 12, 'نام پدر', '5' );
		$test_results[] = App::i()->validate_input( $phone_no, 11, 11, 'شماره تماس', '4' );

		foreach ( $test_results as $key => $value ) {
			if ( true !== $value ) {
				$error_messages[]['error_message'] = $value;
			}
		}

		if ( ! empty( $error_messages ) ) {
			return $error_messages;
		}

		return array();
	}
}
