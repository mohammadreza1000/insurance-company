<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class Insurances {
	private $insurances_model;
	private $users_model;
	private $client_model;

	public function __construct() {
		$this->insurances_model = App::i()->model( 'insurances' );
		$this->users_model      = App::i()->model( 'users' );
		$this->client_model     = App::i()->model( 'clients' );
	}

	public function index() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$result           = $this->insurances_model->get_all_insurances();
		$insurances_array = array();

		while ( $row = $result->fetch_assoc() ) {
			$datetime_c       = new DateTime( $row['datetime'] );
			$datetime         = '0000-00-00 00:00:00' !== $row['datetime'] ?
				App::i()->get_datetime_formatter()->format( $datetime_c ) : '-';
			$datetime2_c      = new DateTime( $row['datetime2'] );
			$datetime2        = '0000-00-00 00:00:00' !== $row['datetime2'] ?
				App::i()->get_datetime_formatter()->format( $datetime2_c ) : '-';
			$added_by_result  = $this->users_model->get_username_by_id( $row['added_by'] );
			$for_whome_result = $this->client_model->get_client_basic_info_by_id( $row['for_whome'] );

			while ( $subrow = $added_by_result->fetch_assoc() ) {
				$added_by = $subrow['username'];
			}

			while ( $subrow = $for_whome_result->fetch_assoc() ) {
				$for_whome = $subrow['firstname'] . ' - ' . $subrow['lastname'] . ' - ' . $subrow['national_code'];
			}

			$insurances_array[] = array(
				'id'        => $row['id'],
				'datetime'  => $datetime,
				'datetime2' => $datetime2,
				'added_by'  => $added_by,
				'for_whome' => $for_whome,
			);
		}

		App::i()->set_view_data( array(
			'insurances' => $insurances_array,
		) );

		App::i()->view();
		App::i()->view( 'home/footer' );
	}

	public function add() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$datetime     = App::i()->filter_input_post( 'datetime' );
		$datetime2    = App::i()->filter_input_post( 'datetime2' );
		$for_whome_nc = App::i()->filter_input_post( 'for_whome_nc' );
		$submit       = App::i()->filter_input_post( 'submit' );

		$view = null;

		$view_data_array = array(
			'datetime_val'     => '',
			'datetime2_val'    => '',
			'for_whome_nc_val' => '',
			'error_messages'   => array(),
		);

		if ( isset( $submit ) && isset( $datetime ) && isset( $datetime2 ) && isset( $for_whome_nc ) ) {
			$error_messages = $this->validate_inputs( $datetime, $datetime2, $for_whome_nc );

			$added_by = intval( $_SESSION['userid'] );

			if ( empty( $error_messages ) ) {
				$for_whome_result    = $this->client_model->get_client_id_by_national_code( $for_whome_nc );
				$datetime_formatted  = date( 'Y-m-d H:i:s', App::i()->get_datetime_formatter()->parse( $datetime ) );
				$datetime2_formatted = date( 'Y-m-d H:i:s', App::i()->get_datetime_formatter()->parse( $datetime2 ) );

				if ( $for_whome_result->num_rows >= 1 && isset( $datetime_formatted ) &&
					! is_null( $datetime_formatted ) && '' != $datetime_formatted && isset( $datetime2_formatted ) &&
					! is_null( $datetime2_formatted ) && '' != $datetime2_formatted
				) {
					while ( $subrow = $for_whome_result->fetch_assoc() ) {
						$for_whome = intval( $subrow['id'] );
					}

					$result = $this->insurances_model->insert_new_insurance(
						$datetime_formatted, $datetime2_formatted, $added_by, $for_whome
					);

					if ( false !== $result ) {
						$view_data_array = array(
							'message' => 'عملیات با موفقیت انجام شد.',
						);
					} else {
						$view_data_array = array(
							'message' => 'خطایی رخ داده است ! شاید شما فیلدهای کلیدی را تکراری وارد کرده‌اید.',
						);
					}

					$view = 'home/message';
				} else {
					$view_data_array = array(
						'message' => 'خطایی رخ داده است از صحت اطلاعات ورودی مطمئن شوید.',
					);

					$view = 'home/message';
				}
			} else {
				$view_data_array = array(
					'datetime_val'     => $datetime,
					'datetime2_val'    => $datetime2,
					'for_whome_nc_val' => $for_whome_nc,
					'error_messages'   => $error_messages,
				);
			}
		}

		App::i()->set_view_data( $view_data_array );
		App::i()->view( $view );
		App::i()->view( 'home/footer' );
	}

	public function delete() {
		App::i()->set_view_data( array(
			'page_title' => 'پنل کاربری',
		) );
		App::i()->view( 'home/header' );

		App::i()->view( 'home/loggedin' );

		$insuranceid = App::i()->filter_input_get( 'id' );

		$result = $this->insurances_model->delete_insurance( $insuranceid );

		if ( false !== $result ) {
			$view_data_array = array(
				'message' => 'عملیات با موفقیت انجام شد.',
			);
		} else {
			$view_data_array = array(
				'message' => 'خطایی رخ داده است !',
			);
		}

		App::i()->set_view_data( $view_data_array );
		App::i()->view( 'home/message' );
		App::i()->view( 'home/footer' );
	}

	protected function validate_inputs( $datetime, $datetime2, $national_code ) {
		$test_results   = array();
		$error_messages = array();

		$test_results[] = App::i()->validate_input( $datetime, 16, 16, 'تاریخ آغاز' );
		$test_results[] = App::i()->validate_input( $datetime2, 16, 16, 'تاریخ اتمام' );
		$test_results[] = App::i()->validate_input( $national_code, 10, 10, 'کد ملی کلاینت', '4' );

		foreach ( $test_results as $key => $value ) {
			if ( true !== $value ) {
				$error_messages[]['error_message'] = $value;
			}
		}

		if ( ! empty( $error_messages ) ) {
			return $error_messages;
		}

		return array();
	}
}
