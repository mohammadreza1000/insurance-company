<?php
if ( ! defined( 'PHP_VALID' ) ) {
	die();
}

class App {
	private $mysqli_link;
	private $root_dir;
	private $root_url;
	private $datetime_formatter;
	private $view_data;
	private $view_name              = '';
	private $current_p_form_handler = '';
	private $controller             = 'home';
	private $method                 = 'index';
	private $params                 = array();
	private static $instance        = null;

	protected function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	public static function i() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function setup( $mysqli_link, $root_dir, $root_url ) {
		$this->mysqli_link = $mysqli_link;

		session_start();

		$this->root_dir = $root_dir;
		$this->root_url = $root_url;
		$url            = $this->parse_inputs();

		$this->datetime_formatter = new IntlDateFormatter(
			'fa_IR@calendar=persian',
			IntlDateFormatter::FULL,
			IntlDateFormatter::FULL,
			'Asia/Tehran',
			IntlDateFormatter::TRADITIONAL,
			'd MMMM yyyy - HH:mm'
		);

		if ( file_exists( $this->get_root_dir() . '/app/controllers/class-' . $url[0] . '.php' ) &&
			isset( $_SESSION['userid'] )
		) {
			$this->controller = $url[0];
		}

		$this->view_name              .= $this->controller;
		$this->current_p_form_handler .= '?c=' . $this->controller;

		require_once $this->get_root_dir() . '/app/controllers/class-' . $this->controller . '.php';

		$this->controller = new $this->controller();

		if ( isset( $url[1] ) && isset( $_SESSION['userid'] ) ) {
			if ( method_exists( $this->controller, $url[1] ) ) {
				$reflection = new ReflectionMethod( $this->controller, $url[1] );
				if ( $reflection->isPublic() ) {
					$this->method = $url[1];
				}
			}
		}

		$this->view_name              .= '/' . $this->method;
		$this->current_p_form_handler .= '&m=' . $this->method;
		$this->params                  = ( isset( $url[2] ) && ! empty( $url[2] ) ) ? array_values( $url[2] ) : array();

		call_user_func_array( [ $this->controller, $this->method ], $this->params );
	}

	public function get_mysqli_link() {
		return $this->mysqli_link;
	}

	public function get_root_dir() {
		return $this->root_dir;
	}

	public function get_root_url() {
		return $this->root_url;
	}

	public function get_datetime_formatter() {
		return $this->datetime_formatter;
	}

	public static function filter_input_get( $input ) {
		return filter_input(
			INPUT_GET,
			$input,
			FILTER_UNSAFE_RAW,
			FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK | FILTER_FLAG_ENCODE_AMP
		);
	}

	public static function filter_input_post( $input ) {
		return filter_input(
			INPUT_POST,
			$input,
			FILTER_UNSAFE_RAW,
			FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_BACKTICK
		);
	}

	public function validate_input( $input, $minlen, $maxlen, $fieldname, $type = '0' ) {
		// type 0 everything, type 1 email, type 2 english chars and nums only, type 3 english chars only,
		// type 4 english numbers only, type 5 persian chars only
		if ( mb_strlen( $input ) < $minlen || mb_strlen( $input ) > $maxlen ) {
			return 'فیلد ' . $fieldname . ' طولانی یا کوتاه وارد شده است.';
		} else {
			if ( '1' === $type &&
				! preg_match( '/^[^0-9][_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $input )
			) {
				return 'آدرس پست الکترونیک معتبر نمی‌باشد.';
			} elseif ( '2' === $type && preg_match( '/[^A-Za-z0-9]/', $input ) ) {
				return 'فیلد ' . $fieldname . ' تنها می‌تواند حاوی حروف و اعداد انگلیسی باشد.';
			} elseif ( '3' === $type && preg_match( '/[^A-Za-z]/', $input ) ) {
				return 'فیلد ' . $fieldname . ' تنها می‌تواند حاوی حروف انگلیسی باشد.';
			} elseif ( '4' === $type && preg_match( '/[^0-9]/', $input ) ) {
				return 'فیلد ' . $fieldname . ' تنها می‌تواند حاوی اعداد انگلیسی باشد.';
			} elseif ( '5' === $type && ! preg_match( '/^([\x{0600}-\x{06EF}| |\x{200C}])+$/u', $input ) ) {
				return 'فیلد ' . $fieldname . ' تنها می‌تواند حاوی حروف فارسی باشد.';
			}
		}

		return true;
	}

	protected function parse_inputs() {
		$output   = array();
		$output[] = strtolower( self::filter_input_get( 'c' ) );
		$output[] = strtolower( self::filter_input_get( 'm' ) );

		return $output;
	}

	public function set_view_data( $data ) {
		$this->view_data = $data;
	}

	public function unset_view_data() {
		unset( $this->view_data );
	}

	protected function convert_template_tags( $array, $view = '' ) {
		foreach ( $array as $key => $value ) {
			if ( is_array( $value ) ) {
				$view = preg_replace_callback( "#\\{{ for:$key \\}}(.*?)\\{{ \/for:$key \\}}#is",
					function( $matches ) use ( $value ) {
						if ( ! empty( $value ) ) {
							if ( array_keys( $value ) !== range( 0, count( $value ) - 1 ) ) {
								return $this->convert_template_tags( $value, $matches[1] );
							} else {
								$sp = '';
								foreach ( $value as $subkey => $subvalue ) {
									$sp .= $this->convert_template_tags( $subvalue, $matches[1] );
								}

								return $sp;
							}
						}

						return '';
					},
				$view );
			} else {
				$view = str_replace( '{{ ' . $key . ' }}', $value, $view );
			}
		}

		return $view;
	}

	public function view( $view_name = null ) {
		if ( is_null( $view_name ) ) {
			$view_name = $this->view_name;
		}

		$file = $this->get_root_dir() . '/app/views/' . $view_name . '.html';

		if ( file_exists( $file ) ) {
			$view = file_get_contents( $file );

			$view = str_replace( '{{ ROOT_URL }}', $this->get_root_url(), $view );
			$view = str_replace( '{{ form_handler }}', $this->current_p_form_handler, $view );

			if ( isset( $this->view_data ) && ! empty( $this->view_data ) ) {
				$view = $this->convert_template_tags( $this->view_data, $view );

				$this->unset_view_data();
			}

			echo $view;
		}
	}

	public function model( $model ) {
		require_once $this->get_root_dir() . '/app/models/class-model-' . $model . '.php';

		$class = 'Model_' . $model;

		return new $class();
	}
}
